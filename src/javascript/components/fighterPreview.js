import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root fighter-preview___general ${positionClassName}`
  });
  fighterElement.insertAdjacentHTML(
    'afterBegin',
    `
      <h1>${fighter.name}</h1> 
      <p>Health: ${fighter.health}</p> 
      <p>Attack: ${fighter.attack}</p>
      <p>Defense: ${fighter.defense}</p>`
  );
  fighterElement.style.background = `url('${fighter.source}') no-repeat`;
  fighterElement.style.backgroundSize = '100%';
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
